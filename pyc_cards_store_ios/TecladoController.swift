//
//  TecladoController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 17/07/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class TecladoController: UIView {
    
    var txtPin: UITextField!
    var btnPagar: UIButton!
    
    @IBOutlet var viewTeclado: UIView!
    @IBOutlet weak var btnBorrar: BotonTeclado!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        NSBundle.mainBundle().loadNibNamed("Teclado", owner: self, options: nil)
        self.viewTeclado.setNeedsUpdateConstraints()
        self.viewTeclado.setNeedsLayout()
        self.addSubview(self.viewTeclado)
    }
    
    //Carga elementos para controlar desde el teclado
    func cargarTeclado(txtPin: UITextField, btnPagar: UIButton){
        self.txtPin = txtPin
        self.btnPagar = btnPagar
        txtPin.layer.cornerRadius = 3
        txtPin.layer.borderColor = CAGradientLayer().UIColorFromHex(0x52CCCC8, alpha: 1.0).CGColor
        txtPin.layer.borderWidth = 3.0
    }
    
    //Pone el texto en el input
    @IBAction func ponerTexto(sender: UIButton) {
        
        if(sender.titleForState(.Normal)! == "<" && count(txtPin.text) > 0)
        {
            self.txtPin.text = self.txtPin.text.substringToIndex(txtPin.text.endIndex.predecessor())

        }
            
        else if(count(txtPin.text) == 5)
        {
            println("Entrando a animar")
            
            self.txtPin.layer.borderWidth = 3.0
            self.txtPin.layer.borderColor = CAGradientLayer().UIColorFromHex(0x00a7f7, alpha: 1).CGColor
            self.txtPin.layer.cornerRadius = 8;
            
            let bounds = self.btnPagar.bounds
            UIView.animateWithDuration(1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: nil, animations: {
                self.btnPagar.bounds = CGRect(x: bounds.origin.x - 20, y: bounds.origin.y, width: bounds.size.width + 60, height: bounds.size.height)
                self.btnPagar.backgroundColor = CAGradientLayer().UIColorFromHex(0x00a7f7, alpha: 1)
                self.btnPagar.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                }, completion: nil)
        }
        
        if(count(txtPin.text) < 6 && sender.titleForState(.Normal)! != "<")
        {
            txtPin.text = txtPin.text + sender.titleForState(.Normal)!
        }
            
        if(count(txtPin.text) < 6)
        {
            txtPin.layer.borderColor = CAGradientLayer().UIColorFromHex(0x52CCCC8, alpha: 1.0).CGColor
            txtPin.layer.borderWidth = 3.0
            self.btnPagar.backgroundColor = CAGradientLayer().UIColorFromHex(0x00a7f7, alpha: 0)
            self.btnPagar.setTitleColor(UIColor.blueColor(), forState: .Normal)
        }
    }
}

//
//  Movimiento.swift
//  PYC_mobile
//
//  Created by Sebastian Vivas on 6/04/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import Foundation

class Movimiento {
    
    var valor: String
    var longitud: Double
    var latitud: Double
    var descripcion: String
    var fecha: String
    
    init (valor: String, longitud: String, latitud: String, descripcion: String, fecha: String)
    {
        self.valor = valor
        self.longitud =  (longitud as NSString).doubleValue
        self.latitud = (latitud as NSString).doubleValue
        self.descripcion = descripcion
        self.fecha = fecha
    }
    
}
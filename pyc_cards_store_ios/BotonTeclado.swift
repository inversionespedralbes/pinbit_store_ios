//
//  BotonTeclado.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 17/07/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import Foundation
import UIKit

class BotonTeclado: UIButton {

    required init(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        var lineView = UIView(frame: CGRectMake(10, 49, self.frame.size.width/2, 1))
        lineView.backgroundColor = .whiteColor() //CAGradientLayer().UIColorFromHex(0x00a7f7, alpha: 1)
        self.addSubview(lineView)
        self.tintColor = CAGradientLayer().UIColorFromHex(0xa7f7, alpha: 1.0)
        self.backgroundColor = .whiteColor()
    }
    
    

}
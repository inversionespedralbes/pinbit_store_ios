//
//  MovimientosController.swift
//  pyc_cards_store_ios
//
//  Created by Sebastian Vivas on 5/08/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class MovimientosController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var headerView: HeaderController!
    @IBOutlet weak var tablaMovimientos: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var listaMovimientos: [Movimiento] = []
    var listaVacia: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initTexts()
        self.addEmptyLabel()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        CAGradientLayer().fondoDegrade(self.headerView, color1: CAGradientLayer().UIColorFromHex(0x0ba0dd, alpha: 1), color2: CAGradientLayer().UIColorFromHex(0x00b8ff, alpha: 1))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        getMovimientos()
        tablaMovimientos.hidden = true
        spinner.startAnimating()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! MovimientosAdapatador
        cell.lblMovimiento.text = listaMovimientos[indexPath.row].descripcion
        cell.lblValor.text = listaMovimientos[indexPath.row].valor
        cell.lblFecha.text = listaMovimientos[indexPath.row].fecha
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaMovimientos.count
    }
    
    //Retorna los movimientos desde el servidor
    func getMovimientos(){
        let deviceID = NSUserDefaults.standardUserDefaults().objectForKey("gcm_id") as! String
        request(.GET, "https://www.pin.com.co/cobros.json?device_id=\(deviceID)&sistema_operativo=iOS")
            .responseJSON { (request, response, datos, error) in
                if(error == nil)
                {
                    if(response?.statusCode == 200)
                    {
                        var json = JSON(datos!).arrayValue
                        for aux in json{
                            self.listaMovimientos.append(Movimiento(valor: aux[""].stringValue, longitud: aux[""].stringValue, latitud: aux[""].stringValue, descripcion: aux[""].stringValue, fecha: aux[""].stringValue))
                        }
                        self.tablaMovimientos.hidden = false
                        self.animateTable()
                    }
                    else
                    {
                        println("Error en carga de movimientos \(datos!)")
                    }
                    println("Tamaño de movimientos \(self.listaMovimientos.count)")
                }
        }
    }
    
    
    //Poner mensaje lista vacia
    func addEmptyLabel()
    {
        if(self.listaMovimientos.count == 0)
        {
            self.listaVacia = UILabel()
            self.listaVacia.frame = CGRect(x:0, y:0, width:self.view.bounds.size.width, height: self.view.bounds.size.height)
            self.listaVacia.text = NSLocalizedString("no_hay_movimientos", comment: "")
            self.listaVacia.textAlignment = .Center
            self.listaVacia.font = UIFont(name: "Palatino-Italic", size: 20)
            self.listaVacia.sizeToFit()
            self.tablaMovimientos.backgroundView = self.listaVacia
            self.tablaMovimientos.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
            self.tablaMovimientos.separatorColor = .whiteColor()
        }
        else
        {
            self.tablaMovimientos.backgroundView = nil
        }
        
    }
    
    //Función que anima la tabla
    func animateTable() {
        tablaMovimientos.reloadData()
        
        let cells = tablaMovimientos.visibleCells()
        let tableHeight: CGFloat = tablaMovimientos.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as! UITableViewCell
            cell.transform = CGAffineTransformMakeTranslation(0, tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as! UITableViewCell
            UIView.animateWithDuration(0.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: nil, animations: {
                cell.transform = CGAffineTransformMakeTranslation(0, 0);
                }, completion: nil)
            
            index += 1
        }
    }
    
    //Inicia los textos de la vista
    func initTexts(){
        self.headerView.lblTitulo.text = NSLocalizedString("movimientos", comment: "")
        self.headerView.lblNombre.text = NSUserDefaults.standardUserDefaults().stringForKey("nombre")
        self.headerView.lblSaldo.text = NSUserDefaults.standardUserDefaults().stringForKey("equipo")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

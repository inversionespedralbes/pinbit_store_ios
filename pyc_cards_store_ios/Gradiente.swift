//
//  Gradiente.swift
//  PYC_mobile
//
//  Created by Sebastian Vivas on 11/05/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

extension CAGradientLayer{
    
    
    func fondoGris(cell: UITableViewCell) -> CAGradientLayer{
        let topColor = UIColor(red: 237/255.0, green: 237/255.0, blue: 237/255.0, alpha: 1)
        let bottomColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
        let gradientColors: [CGColor] = [topColor.CGColor, bottomColor.CGColor]
        let gradientLocations: [Float] = [0.0,1.0]
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocations
        gradientLayer.frame  = cell.bounds
        return gradientLayer
    }
    
    func fondoAzul(cell: UITableViewCell) -> CAGradientLayer{
        let topColor = UIColor(red: 0/255.0, green: 85/255.0, blue: 157/255.0, alpha: 1)
        let bottomColor = UIColor(red: 0/255.0, green: 85/255.0, blue: 157/255.0, alpha: 1)
        let gradientColors: [CGColor] = [topColor.CGColor, bottomColor.CGColor]
        let gradientLocations: [Float] = [0.0,1.0]
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocations
        gradientLayer.frame  = cell.bounds
        return gradientLayer
    }
    
    func fondoAzulDegrade(cell: UIView) -> CAGradientLayer{
        let topColor = UIColor(red: 11/255.0, green: 160/255.0, blue: 221/255.0, alpha: 1)
        let bottomColor = UIColor(red: 0/255.0, green: 184/255.0, blue: 255/255.0, alpha: 1)
        let gradientColors: [CGColor] = [topColor.CGColor, bottomColor.CGColor]
        let gradientLocations: [Float] = [0.0,1.0]
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocations
        gradientLayer.frame  = cell.bounds
        return gradientLayer
    }
    
    func fondoDegrade(view: UIView, color1: UIColor, color2: UIColor){
        view.layoutIfNeeded()
        var gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = view.frame
        gradient.colors = [color1.CGColor, color2.CGColor]
        view.layer.insertSublayer(gradient, atIndex: 0)
    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double!)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
}

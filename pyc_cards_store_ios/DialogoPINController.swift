//
//  DialogoSimpleController.swift
//  PYC_mobile
//
//  Created by Sebastian Vivas on 4/05/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class DialogoPINController: UIViewController {

    @IBOutlet weak var vistaDialogo: UIView!
    @IBOutlet weak var imgQR: UIImageView!
    @IBOutlet weak var lblPIN: UILabel!
    @IBOutlet weak var btnCerrar: UIButton!
    
    
    var qrcodeImage: CIImage!
    var latitud: Double!
    var longitud: Double!
    var pin: String!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        self.vistaDialogo.layer.cornerRadius = 5
        self.vistaDialogo.layer.shadowOpacity = 0.8
        self.vistaDialogo.layer.shadowOffset = CGSizeMake(0.0, 0.0)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        let imageName = "info.png"
        let imageView = UIImageView(image: UIImage(named: imageName)!)
        
        let viewBg = UIView()
        viewBg.frame = CGRectMake( (UIScreen.mainScreen().bounds.width-21)/2, 95, 25,  25)
        viewBg.layer.cornerRadius = viewBg.frame.height/2
        viewBg.backgroundColor = UIColor.whiteColor()
        self.view.addSubview(viewBg)
        
        //imageView.frame = CGRectMake( (UIScreen.mainScreen().bounds.width-21)/2, 87, 21,  21)
        imageView.frame = CGRectMake( (viewBg.bounds.width-21)/2, 2, 21,  21)
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = CAGradientLayer().UIColorFromHex(0x00559d, alpha: 1.0).CGColor
        imageView.layer.cornerRadius = imageView.frame.height/2
        viewBg.addSubview(imageView)
    }
    
    //Inicializa el dialogo
    func showInView(aView: UIView!, pin: String!, animated: Bool)
    {
        aView.addSubview(self.view)
        self.view.frame = aView.frame
        self.pin = pin
        self.crearQR()
        if animated
        {
            self.showAnimate()
        }
    }
    
    //Muestra el dialogo de forma animada
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    //Quita el dialogo de forma animada
    func removeAnimate()
    {
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    //Crear un código QR para el PIN
    func crearQR(){
        let data = pin.dataUsingEncoding(NSISOLatin1StringEncoding, allowLossyConversion: false)
        
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        filter.setValue(data, forKey: "inputMessage")
        filter.setValue("Q", forKey: "inputCorrectionLevel")
        
        qrcodeImage = filter.outputImage
        let scaleX = imgQR.frame.size.width / qrcodeImage.extent().size.width
        let scaleY = imgQR.frame.size.height / qrcodeImage.extent().size.height
        
        let transformedImage = qrcodeImage.imageByApplyingTransform(CGAffineTransformMakeScale(scaleX, scaleY))
        
        imgQR.image = UIImage(CIImage: transformedImage)
        btnCerrar.layer.masksToBounds = true
        btnCerrar.layer.cornerRadius = 3
        btnCerrar.setTitle(NSLocalizedString("cerrar", comment: ""), forState: .Normal)
        self.lblPIN.text = NSLocalizedString("pinbit", comment: "") +  self.pin
        //imgQR.image = UIImage(CIImage: qrcodeImage)
    }

    @IBAction func btnAction(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("cerrarPIN", object: nil)
        removeAnimate()
    }
}

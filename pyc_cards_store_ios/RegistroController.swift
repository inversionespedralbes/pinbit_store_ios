//
//  RegistroController.swift
//  pyc_cards_store_ios
//
//  Created by Sebastian Vivas on 5/08/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class RegistroController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var vistaHeader: HeaderController!
    @IBOutlet weak var txtID: UITextField!
    @IBOutlet weak var txtNombreEquipo: UITextField!
    @IBOutlet weak var segmentID: UISegmentedControl!
    @IBOutlet weak var btnRegistrar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initTextos()
        self.txtID.becomeFirstResponder()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        CAGradientLayer().fondoDegrade(self.vistaHeader, color1: CAGradientLayer().UIColorFromHex(0x0ba0dd, alpha: 1), color2: CAGradientLayer().UIColorFromHex(0x00b8ff, alpha: 1))
    }
    
    
    
    //Detecta toques en pantalla
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    func initTextos(){
        vistaHeader.lblTitulo.text =  NSLocalizedString("registro", comment: "")
        vistaHeader.lblSaludo.text = NSLocalizedString("datos_registro",comment: "")
        vistaHeader.lblSaludo.hidden = true
        vistaHeader.lblSaldo.hidden = true
        vistaHeader.lblTuTarjeta.hidden = true
        vistaHeader.lblNombre.hidden = true
        txtID.placeholder = NSLocalizedString("ingresa_id", comment: "")
        txtNombreEquipo.placeholder = NSLocalizedString("ingresa_equipo",comment: "")
        btnRegistrar.setTitle(NSLocalizedString("registrar", comment: ""), forState: .Normal)
        segmentID.setTitle(NSLocalizedString("cedula", comment: ""), forSegmentAtIndex: 0)
        segmentID.setTitle(NSLocalizedString("nit", comment: ""), forSegmentAtIndex: 1)
        
        let lblSub = UILabel()
        lblSub.frame = CGRectMake(vistaHeader.lblSaludo.frame.origin.x, vistaHeader.lblSaludo.frame.origin.y-5, 250, 20)
        lblSub.text = NSLocalizedString("datos_registro", comment: "")
        lblSub.font = UIFont(name : lblSub.font.fontName, size: 17)
        lblSub.textColor = UIColor.whiteColor()

        vistaHeader.addSubview(lblSub)
        
        
    }
    
    @IBAction func btnRegistrar(sender: AnyObject) {
        if(txtID.text == "" || txtNombreEquipo.text == "")
        {
            if(txtID.text == "" ){
                animarTextField(txtID)
            }
            else{
                animarTextField(txtNombreEquipo)
            }
        }
        else{
            registrarDispositivo()
        }
        
        
    }
    
    func registrarDispositivo(){
        var tipoDoc: String!
        
        if(segmentID.selectedSegmentIndex == 0) {
            tipoDoc = "2"
        }
        else {
            tipoDoc = "1"
        }
        
        let url = "https://www.pin.com.co/dispositivos.json"
        let deviceID: AnyObject? = NSUserDefaults.standardUserDefaults().objectForKey("gcm_id")
        let parameters = [
            "dispositivo": [
                "device_id":"\(deviceID!)",
                "codigo": "\(txtNombreEquipo.text)",
                "sistema_operativo": "iOS"],
            "tipo_documento_id": tipoDoc,
            "numero_documento": "\(txtID.text)"
        ]
        
        request(.POST, url, parameters: parameters as? [String : AnyObject]).responseJSON{ (request, response, resp, error) -> Void in
            if(error == nil)
            {
                if(response?.statusCode == 201){
                    NSUserDefaults.standardUserDefaults().setObject("si", forKey: "registrado")
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
                else{
                    println("Error en el registro \(resp)")
                }
            }
            else
            {
                //Error en el servidor
            }
        }
    }
    
    //Animación de text fields
    func animarTextField(txtAnimar: UITextField){
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(txtAnimar.center.x - 5, txtAnimar.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(txtAnimar.center.x + 5, txtAnimar.center.y))
        txtAnimar.layer.addAnimation(animation, forKey: "position")
    }

}

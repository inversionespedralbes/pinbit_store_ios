//
//  ViewController.swift
//  pyc_cards_store_ios
//
//  Created by Sebastian Vivas on 30/07/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var headerView: HeaderController!
    @IBOutlet weak var viewTeclado: TecladoController!
    @IBOutlet weak var txtValor: UITextField!
    @IBOutlet weak var btnGenerar: UIButton!
    @IBOutlet weak var lblIngresa: UILabel!
    
    var registrado: Bool!
    
    var dialogoPIN: DialogoPINController!
    var dialogoSimple: DialogoSimpleController!
    var dialogoCarga: DialogoCargaController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "cerrarPIN:", name:"cerrarPIN", object: nil)
        registrado = false
        viewTeclado.cargarTeclado(txtValor, btnPagar: btnGenerar)
        
        //Verifica que el dispositivo ya este registrado
        if let name = NSUserDefaults.standardUserDefaults().stringForKey("registrado")
        {
            registrado = true
            getDatos()
        }
        else
        {
            
        }
        self.iniTexts()
    }
    
    override func viewDidAppear(animated: Bool) {
        //self.getDatos()
        if(!registrado)
        {
            performSegueWithIdentifier("registro", sender: self)
        }
        else
        {
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        CAGradientLayer().fondoDegrade(self.headerView, color1: CAGradientLayer().UIColorFromHex(0x0ba0dd, alpha: 1), color2: CAGradientLayer().UIColorFromHex(0x00b8ff, alpha: 1))
    }
    
    //Carga los datos para la caja
    func getDatos(){
        let deviceID: AnyObject? = NSUserDefaults.standardUserDefaults().objectForKey("gcm_id")
        let url = "https://www.pin.com.co/dispositivos/" + String(stringInterpolationSegment: deviceID!) + ".json?sistema_operativo=iOS"
        
        request(.GET, url).responseJSON {
            (request, response, resp, error) -> Void in
            if(error == nil)
            {
                println("\(resp!)")
                if(response?.statusCode == 200){
                    var json: JSON = JSON(resp!)
                    NSUserDefaults.standardUserDefaults().setObject(json["info"]["codigo"].stringValue, forKey: "equipo")
                    NSUserDefaults.standardUserDefaults().setObject(json["info"]["establecimiento"].stringValue, forKey: "nombre")
                }
                else {
                    
                }
            }
        }
    }
    
    //Acción para botón crear PINBIT
    @IBAction func btnCrearPin(sender: AnyObject) {
        
        if(txtValor.text == "")
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(CGPoint: CGPointMake(txtValor.center.x - 5, txtValor.center.y))
            animation.toValue = NSValue(CGPoint: CGPointMake(txtValor.center.x + 5, txtValor.center.y))
            txtValor.layer.addAnimation(animation, forKey: "position")
        }
        else
        {
            dialogoCarga = DialogoCargaController(nibName: "DialogoCarga", bundle: nil)
            dialogoCarga.showInView(self.view, titulo: NSLocalizedString("creando", comment: ""), animated: true)
            crearPinBit()
        }
        
    }
    
    //Crea un rquest para pagar
    func crearPinBit (){
        let deviceID = NSUserDefaults.standardUserDefaults().objectForKey("gcm_id") as! String
        
        let url = "https://www.pin.com.co/pins.json"
        let parameters = [
            "valor":txtValor.text,
            "device_id": deviceID,
            "latitud_cobro":"",
            "longitud_cobro":"",
            "sistema_operativo":"iOS"
        ]
        
        request(.POST, url, parameters: parameters).responseJSON {
            (request, response, resp, error) -> Void in
            if(error == nil)
            {
                self.dialogoCarga.removeAnimate()
                if(response?.statusCode == 201)
                {
                    var json: JSON = JSON(resp!)
                    println(json)
                    let pin = json["pin"].stringValue
                    self.dialogoPIN = DialogoPINController(nibName: "DialogoPIN", bundle: nil)
                    self.dialogoPIN.showInView(self.view, pin: pin, animated: true)
                }
                else
                {
                    println("Error en el server \(resp!)")
                    self.dialogoSimple = DialogoSimpleController(nibName: "DialogoSimple", bundle: nil)
                    self.dialogoSimple.showInView(self.view, titulo: NSLocalizedString("pinbit", comment: ""), cuerpo: NSLocalizedString("no_pin", comment: ""), animated: true)
                }
            }
            else
            {
                //Error en la conexion
            }
        }
    }
    
    //Inicia los textos de la vista
    func iniTexts(){
        lblIngresa.text = NSLocalizedString("ingresar_valor", comment: "")
        btnGenerar.setTitle(NSLocalizedString("generar_pinbit", comment: ""), forState: .Normal)
        headerView.lblTitulo.text = NSLocalizedString("crear_pin", comment: "")
        if let name = NSUserDefaults.standardUserDefaults().stringForKey("nombre")
        {
            headerView.lblNombre.text = NSUserDefaults.standardUserDefaults().stringForKey("nombre")
            headerView.lblSaldo.text = NSUserDefaults.standardUserDefaults().stringForKey("equipo")
        }
        else
        {
            
        }
        
    }
    
    //Eliminar el texto de text field cuando se cierra el dialogo de generación de PIN
    @objc func cerrarPIN(notification: NSNotification){
        txtValor.text = ""
    }
}


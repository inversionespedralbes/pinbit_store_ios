//
//  DialogoSimpleController.swift
//  PYC_mobile
//
//  Created by Sebastian Vivas on 4/05/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class DialogoSimpleController: UIViewController {

    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var lblContenido: UILabel!
    @IBOutlet weak var vistaDialogo: UIView!
    @IBOutlet weak var btnAceptar: UIButton!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        self.vistaDialogo.layer.cornerRadius = 5
        self.vistaDialogo.layer.shadowOpacity = 0.8
        self.vistaDialogo.layer.shadowOffset = CGSizeMake(0.0, 0.0)
        btnAceptar.layer.masksToBounds = true
        btnAceptar.layer.cornerRadius = 3
        
    }
    
    func showInView(aView: UIView!, titulo: String!, cuerpo: String!, animated: Bool)
    {
        aView.addSubview(self.view)
        self.view.frame = aView.frame
        lblTitulo.text = titulo
        lblContenido.text = cuerpo
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }

    @IBAction func btnAction(sender: AnyObject) {
        removeAnimate()
    }
}

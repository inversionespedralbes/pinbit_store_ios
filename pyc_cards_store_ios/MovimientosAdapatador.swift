//
//  MovimientosAdapatador.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 9/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class MovimientosAdapatador: UITableViewCell {

    @IBOutlet weak var lblFecha: UILabel!
    @IBOutlet weak var lblMovimiento: UILabel!
    @IBOutlet weak var imagen: UIImageView!
    @IBOutlet weak var lblValor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

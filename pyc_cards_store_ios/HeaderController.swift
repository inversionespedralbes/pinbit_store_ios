//
//  HeaderController.swift
//  PYC_mobile
//
//  Created by Sebastian Vivas on 28/04/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class HeaderController: UIView {

    @IBOutlet var view: HeaderController!
    @IBOutlet weak var lblSaldo: UILabel!
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var btnSaldo: UIButton!
    @IBOutlet weak var lblSaludo: UILabel!
    @IBOutlet weak var lblTuTarjeta: UILabel!
    //@IBOutlet weak var btnCambiarTarjeta: UIButton!
    @IBOutlet weak var lblTitulo: UILabel!
    
    var actualizo: Bool! = false
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        NSBundle.mainBundle().loadNibNamed("Header", owner: self, options: nil)
        self.addSubview(self.view)
        
        self.llenarTextos()
        
    }
    
    //Inicializar textos
    func llenarTextos(){
        lblSaludo.text = NSLocalizedString("hola", comment: "")
        lblTuTarjeta.text = NSLocalizedString("equipo", comment: "")
    }
    
    
}
    

